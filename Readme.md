# Frequencies GRU encoder-decoder from scratch
## Task
Fill an input frequency and continue sequence from the last passed to encoder point   
![Desired results](img/desired.png "Desired results example")

## Dataset
10000 of different frequencies in range 10-110 Hz with step 0.01 Hz

## Train/Test data split
Model trained on frequencies' ranges 10-25 Hz, 30-85 Hz, 90-110 Hz  
Model tested on unsean frequencies' ranges 25-30 Hz and 85-90 Hz 
   
![Frequencies' ranges](img/dataset.png "Dataset split")

## Evaluation
MSE 2.435159683227539 was riched it's equal to 0,000260083 absolute error per sample with sample rate 6000 Hz  
![Model evaluation](img/results.png "Model evaluation results")
